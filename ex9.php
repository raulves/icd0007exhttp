<?php

require_once 'lib/tpl.php';

$cmd = param('cmd') ? param('cmd') : 'form';

$data = [];

if ($cmd === 'form') {
    $data['$path'] = 'tpl/form.html';
} else if ($cmd === 'success') {
    $data['$path'] = 'tpl/success.html';
} else if ($cmd === 'save') {

    $answer = param('answer');

    if (intval($answer) === 10) {
        header('Location: ?cmd=success');
        return;
    } else {
        $data['$path'] = 'tpl/form.html';
        $data['$error'] = 'Proovige uuesti!';
        $data['$answer'] = $answer;
    }
} else {
    throw new Exception('invalid command: ' . $cmd);
}

print render_template('tpl/main.html', $data);




function param($key) {
    if (isset($_GET[$key])) {
        return $_GET[$key];
    } else if (isset($_POST[$key])) {
        return $_POST[$key];
    } else {
        return '';
    }
}

