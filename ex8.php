<?php


if (param('cmd') === 'ok') {
    print "OK";
} else if(param('cmd') === 'redirect') {
    header('Location: target.html');
}




function param($key) {
    if (isset($_GET[$key])) {
        return $_GET[$key];
    } else if (isset($_POST[$key])) {
        return $_POST[$key];
    } else {
        return '';
    }
}
